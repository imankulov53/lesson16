package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity

public class Client implements Serializable {

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "seq_client_id", sequenceName = "seq_client_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_client_id")
    int idClient;

    @Column(name = "name")
    String nameClient;

    @Column(name = "address")
    String addressClient;

    @OneToMany(mappedBy =  "idOrder",fetch = FetchType.EAGER)
    Set<Order> orders = new HashSet<>();

    public Client(){}

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getNameClient() {
        return nameClient;
    }

    public void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    public String getAddressClient() {
        return addressClient;
    }

    public void setAddressClient(String addressClient) {
        this.addressClient = addressClient;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }
}
