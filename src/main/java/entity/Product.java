package entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "product")
public class Product {

    @Id
    @Column(name = "id")
    int idProduct;

    @Column(name = "name")
    String nameProduct;

    @Column(name = "price")
    int priceProduct;

    @ManyToOne(fetch = FetchType.EAGER)
            @JoinColumn(name = "manufacturer_id")
    Manufacturer manufacturerID;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "ProductOrders",
            joinColumns = { @JoinColumn(name ="id_product") },
            inverseJoinColumns = { @JoinColumn(name = "id_order") }
    )
    List<Order> orders = new ArrayList<>();

    public Product(){}

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public int getPriceProduct() {
        return priceProduct;
    }

    public void setPriceProduct(int priceProduct) {
        this.priceProduct = priceProduct;
    }

    public Manufacturer getManufacturerID() {
        return manufacturerID;
    }

    public void setManufacturerID(Manufacturer manufacturerID) {
        this.manufacturerID = manufacturerID;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return idProduct == product.idProduct && Objects.equals(nameProduct, product.nameProduct) && Objects.equals(manufacturerID, product.manufacturerID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idProduct, nameProduct, manufacturerID);
    }
}
