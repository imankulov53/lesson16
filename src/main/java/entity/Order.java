package entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Orders")
public class Order {

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "seq_order_id", sequenceName = "seq_order_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_order_id")
    int idOrder;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_client")
    Client idOfClient;

    @Column(name = "order_time")
    Timestamp orderTime;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "ProductOrders",
            joinColumns = { @JoinColumn(name ="id_order") },
            inverseJoinColumns = { @JoinColumn(name = "id_product") }
    )
    List<Product> products = new ArrayList<>();

    public Order(){}

    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public Client getIdOfClient() {
        return idOfClient;
    }

    public void setIdOfClient(Client idOfClient) {
        this.idOfClient = idOfClient;
    }

    public Timestamp getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Timestamp orderTime) {
        this.orderTime = orderTime;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
