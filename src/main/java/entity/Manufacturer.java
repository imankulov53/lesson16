package entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Manufacturer implements Serializable {

    @Id
    @Column(name = "id")
    @SequenceGenerator(name = "seq_manufacturer_id", sequenceName = "seq_manufacturer_id", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_manufacturer_id")
    int id;

    @Column(name = "name")
    String name;

    @Column(name = "site_link")
    String siteLink;

    @OneToMany(mappedBy = "manufacturerID", fetch = FetchType.EAGER)
    Set<Product> products = new HashSet<>();

    public Manufacturer(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSiteLink() {
        return siteLink;
    }

    public void setSiteLink(String siteLink) {
        this.siteLink = siteLink;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}
