import entity.Client;
import entity.Manufacturer;
import entity.Order;
import entity.Product;
import services.ClientService;
import services.ManufacturerService;
import services.OrderService;
import services.ProductService;

import java.sql.Timestamp;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Main {
    private static ProductService productService = new ProductService();
    private static OrderService orderService = new OrderService();
    private static ManufacturerService manufacturerService = new ManufacturerService();
    private static ClientService clientService = new ClientService();

    public static void main(String[] args) {
//        insertClient();
//        insertManufacturer();
//        insertProduct();
//        insertOrder();
        topProduct(2);

    }
    //CREATE METHODS
    public static void insertClient(){
        Client client = new Client();
        Set<Order> orders = new HashSet<>();
        orders.add(orderService.findById(1));
        client.setNameClient("Vlad");
        client.setOrders(orders);
        client.setAddressClient("Street 65. Almaty");

        Client client1 = new Client();
        Set<Order> orders1 = new HashSet<>();
        orders.add(orderService.findById(2));
        client1.setNameClient("Natasha");
        client1.setOrders(orders1);
        client1.setAddressClient("Street 66. Nur-Sultan");
        clientService.persist(client);
        clientService.persist(client1);
    }

    public static void insertManufacturer(){
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setName("Apple");
        manufacturer.setSiteLink("https://apple.com");

        Manufacturer manufacturer1 = new Manufacturer();
        manufacturer1.setName("Samsung");
        manufacturer1.setSiteLink("https://samsung.com");
        manufacturerService.persist(manufacturer);
        manufacturerService.persist(manufacturer1);
    }

    public static void insertOrder(){
        Order order = new Order();

        List<Product> products = new ArrayList<>();
        products.add(productService.findById(1));
        products.add(productService.findById(2));

        order.setOrderTime(new Timestamp(new Date().getTime()));
        order.setIdOfClient(clientService.findById(1));
        order.setProducts(products);

        Order order1 = new Order();

        List<Product> products1 = new ArrayList<>();
        products.add(productService.findById(3));
        products.add(productService.findById(3));

        order1.setOrderTime(new Timestamp(new Date().getTime()));
        order1.setIdOfClient(clientService.findById(2));
        order1.setProducts(products1);

        orderService.persist(order);
        orderService.persist(order1);
    }

    public static void insertProduct(){
        Product product = new Product();

        product.setNameProduct("Iphone X");
        product.setManufacturerID(manufacturerService.findById(1));
        product.setIdProduct(1);
        product.setPriceProduct(250000);

        Product product1 = new Product();
        product1.setNameProduct("Iphone XR");
        product1.setManufacturerID(manufacturerService.findById(1));
        product1.setIdProduct(2);
        product1.setPriceProduct(150000);

        Product product2 = new Product();
        product2.setNameProduct("Samsung S20");
        product2.setManufacturerID(manufacturerService.findById(2));
        product2.setIdProduct(3);
        product2.setPriceProduct(300000);

        productService.persist(product);
        productService.persist(product1);
        productService.persist(product2);
    }

    //READ METHODS
    public static Client getClient(int index){
        Client client = clientService.findById(index);
        return client;
    }

    public static Manufacturer getManufacturerService(int index){
        Manufacturer manufacturer = manufacturerService.findById(index);
        return manufacturer;
    }

    public static Order getOrder(int index){
       Order order = orderService.findById(index);
       return order;
    }

    public static Product getProduct(int index){
        Product product = productService.findById(index);
        return product;
    }

    //UPDATE METHODS
    public static void updateClient(Client client){
        clientService.update(client);
    }

    public static void updateManufacturerService(Manufacturer manufacturer){
        manufacturerService.update(manufacturer);
    }

    public static void updateOrder(Order order){
        orderService.update(order);
    }

    public static void updateProduct(Product product){
       productService.update(product);
    }

    //DELETE METHODS
    public static void deleteClient(Client client){
        clientService.delete(client.getIdClient());
    }

    public static void deleteManufacturerService(Manufacturer manufacturer){
        manufacturerService.delete(manufacturer.getId());
    }

    public static void deleteOrder(Order order){
        orderService.delete(order.getIdOrder());
    }

    public static void deleteProduct(Product product){
        productService.delete(product.getIdProduct());
    }

    //TOP PRODUCTS
    public static void topProduct(int index) {
        List<Product> list = new ArrayList<>();
            clientService.findAll().forEach(client -> client.getOrders().forEach(order -> order.getProducts().forEach(product -> list.add(product))));

    TreeMap<Long, Product> topProducts = new TreeMap<>(Collections.reverseOrder());
        list.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
            .entrySet().stream()
                .forEach(productLongEntry -> topProducts.put(productLongEntry.getValue(),productLongEntry.getKey()));

        topProducts.entrySet().stream().limit(index).forEach(p ->System.out.println(p.getValue().getNameProduct() + " " + p.getKey()));
    }

    //PRODUCTS BETWEEN DATES

}
