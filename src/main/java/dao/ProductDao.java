package dao;

import entity.Product;

import java.util.List;

public class ProductDao extends OpenDao<Product,Integer> {
    @Override
    public void persist(Product entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Product entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public Product findById(Integer integer) {
        return getCurrentSession().get(Product.class,integer);
    }

    @Override
    public void delete(Product entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Product> findAll() {
        return getCurrentSession().createQuery(" FROM Product").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(product -> delete(product));
    }
}
