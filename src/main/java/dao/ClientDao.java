package dao;

import entity.Client;

import java.util.List;

public class ClientDao extends OpenDao<Client,Integer>{
    @Override
    public void persist(Client entity) {
        getCurrentSession().save(entity);
//        getCurrentSession().close();
    }

    @Override
    public void update(Client entity) {
        getCurrentSession().saveOrUpdate(entity);
//        getCurrentSession().close();
    }

    @Override
    public Client findById(Integer integer) {
        return getCurrentSession().get(Client.class,integer);
    }

    @Override
    public void delete(Client entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Client> findAll() {
        return getCurrentSession().createQuery("FROM Client").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(client -> delete(client));
    }
}
