package dao;

import entity.Order;

import java.util.List;

public class OrderDao extends OpenDao<Order,Integer> {
    @Override
    public void persist(Order entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Order entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public Order findById(Integer integer) {
        return getCurrentSession().get(Order.class, integer);
    }

    @Override
    public void delete(Order entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Order> findAll() {
        return getCurrentSession().createQuery(" FROM Order").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(order -> delete(order));
    }
}
