package dao;

import entity.Manufacturer;

import java.util.List;

public class ManufacturerDao extends OpenDao<Manufacturer,Integer> {
    @Override
    public void persist(Manufacturer entity) {
        getCurrentSession().save(entity);
//        getCurrentSession().close();
    }

    @Override
    public void update(Manufacturer entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public Manufacturer findById(Integer integer) {
        return getCurrentSession().get(Manufacturer.class,integer);
    }

    @Override
    public void delete(Manufacturer entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Manufacturer> findAll() {
        return getCurrentSession().createQuery("FROM Manufacturer").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(manufacturer -> delete(manufacturer));
    }
}
