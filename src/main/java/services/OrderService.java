package services;

import dao.Dao;
import dao.OrderDao;
import entity.Order;

import java.util.List;

public class OrderService {

    private static Dao<Order, Integer> orderDao;

    public OrderService() {
        orderDao = new OrderDao();
    }

    public void persist(Order order){
        orderDao.openSession();
        orderDao.persist(order);
        orderDao.closeSession();
    }

    public void update(Order order){
        orderDao.openSession();
        orderDao.update(order);
        orderDao.closeSession();
    }

    public Order findById(Integer integer){
        orderDao.openSession();
        Order order = orderDao.findById(integer);
        orderDao.closeSession();
        return order;
    }

    public void delete(Integer integer){
        orderDao.openSession();
        orderDao.delete(findById(integer));
        orderDao.closeSession();
    }

    public List<Order> findAll(){
        orderDao.openSession();
        List<Order> list = orderDao.findAll();
        orderDao.closeSession();
        return list;
    }

    public void deleteAll(){
        orderDao.openSession();
        orderDao.deleteAll();
        orderDao.closeSession();
    }
}
