package services;

import dao.Dao;
import dao.ManufacturerDao;
import entity.Manufacturer;

import java.util.List;

public class ManufacturerService {

    private static Dao<Manufacturer,Integer> manufacturerDao;

    public ManufacturerService() {
        manufacturerDao = new ManufacturerDao();
    }

    public void persist(Manufacturer manufacturer){
        manufacturerDao.openSession();
        manufacturerDao.persist(manufacturer);
        manufacturerDao.closeSession();
    }

    public void update(Manufacturer manufacturer){
        manufacturerDao.openSession();
        manufacturerDao.update(manufacturer);
        manufacturerDao.closeSession();
    }

    public Manufacturer findById(Integer integer){
        manufacturerDao.openSession();
        Manufacturer manufacturer = manufacturerDao.findById(integer);
        manufacturerDao.closeSession();
        return manufacturer;
    }

    public void delete(Integer integer){
        manufacturerDao.openSession();
        manufacturerDao.delete(findById(integer));
        manufacturerDao.closeSession();
    }

    public List<Manufacturer> findAll(){
        manufacturerDao.openSession();
        List<Manufacturer> list = manufacturerDao.findAll();
        manufacturerDao.closeSession();
        return list;
    }

    public void deleteAll(){
        manufacturerDao.openSession();
        manufacturerDao.deleteAll();
        manufacturerDao.closeSession();
    }
}
