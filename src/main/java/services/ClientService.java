package services;

import dao.ClientDao;
import dao.Dao;
import entity.Client;

import java.util.List;

public class ClientService {

    private static Dao<Client,Integer> clientDao;

    public ClientService() {
        clientDao = new ClientDao();
    }

    public void persist(Client client){
        clientDao.openSession();
        clientDao.persist(client);
        clientDao.closeSession();
    }

    public void update(Client client){
        clientDao.openSession();
        clientDao.update(client);
        clientDao.closeSession();
    }

    public Client findById(Integer integer){
        clientDao.openSession();
        Client client = clientDao.findById(integer);
        clientDao.closeSession();
        return client;
    }

    public void delete(Integer integer){
        clientDao.openSession();
        clientDao.delete(findById(integer));
        clientDao.closeSession();
    }

    public List<Client> findAll(){
    clientDao.openSession();
    List<Client> list = clientDao.findAll();
    clientDao.closeSession();
    return list;
    }

    public void deleteAll(){
        clientDao.openSession();
        clientDao.deleteAll();
        clientDao.closeSession();
    }
}
