package services;

import dao.Dao;
import dao.ProductDao;
import entity.Product;

import java.util.List;

public class ProductService {

    private static Dao<Product, Integer> productDao;

    public ProductService() {
        productDao = new ProductDao();
    }

    public void persist(Product product){
        productDao.openSession();
        productDao.persist(product);
        productDao.closeSession();
    }

    public void update(Product product){
        productDao.openSession();
        productDao.update(product);
        productDao.closeSession();
    }

    public Product findById(Integer integer){
        productDao.openSession();
        Product product = productDao.findById(integer);
        productDao.closeSession();
        return product;
    }

    public void delete(Integer integer){
        productDao.openSession();
        productDao.delete(findById(integer));
        productDao.closeSession();
    }

    public List<Product> findAll(){
        productDao.openSession();
        List<Product> list = productDao.findAll();
        productDao.closeSession();
        return list;
    }

    public void deleteAll(){
        productDao.openSession();
        productDao.deleteAll();
        productDao.closeSession();
    }
}
