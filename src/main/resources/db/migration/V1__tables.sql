CREATE SEQUENCE seq_client_id;
CREATE SEQUENCE seq_order_id;
CREATE SEQUENCE seq_manufacturer_id;

CREATE TABLE Client(
    id int DEFAULT nextval('seq_client_id') PRIMARY KEY ,
    name varchar(20),
    address varchar(100)
);

CREATE TABLE Manufacturer(
    id int DEFAULT nextval('seq_manufacturer_id')  PRIMARY KEY,
    name varchar(30),
    site_link varchar(50)
);

CREATE TABLE Orders(
    id int DEFAULT nextval('seq_order_id')  PRIMARY KEY,
    id_client int,
    order_time TIMESTAMP NOT NULL
);

CREATE TABLE Product(
    id int PRIMARY KEY,
    name varchar(20),
    price int NOT NULL,
    manufacturer_id int NOT NULL
);

CREATE TABLE ProductOrders(
    id_order int NOT NULL,
    id_product int NOT NULL
);


ALTER TABLE Orders
    ADD FOREIGN KEY (id_client) REFERENCES Client(id);

ALTER TABLE Product
     ADD FOREIGN KEY (manufacturer_id) REFERENCES Manufacturer(id);

ALTER TABLE ProductOrders ADD FOREIGN KEY (id_order) REFERENCES Orders(id);
ALTER TABLE ProductOrders ADD FOREIGN KEY (id_product) REFERENCES Product(id);
